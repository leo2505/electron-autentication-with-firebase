import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { MessagingService } from '../shared/messaging.service';

@Component({
  selector: 'app-tela-principal',
  templateUrl: './tela-principal.component.html',
  styleUrls: ['./tela-principal.component.css']
})
export class TelaPrincipalComponent implements OnInit {

  //constructor(private  authService:  AuthService, private messagingService: MessagingService) { }
  constructor( public authService:  AuthService, public messagingService: MessagingService) { }
  message;
  
  ngOnInit() {
    const userId = 'user001';
    this.messagingService.requestPermission(userId)
    this.messagingService.receiveMessage()
    this.message = this.messagingService.currentMessage
  }
}
