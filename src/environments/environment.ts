// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCAgZvhIXoxRR8BmUsnpEGVX5kMdVd3EYA",
    authDomain: "prototipo-electron.firebaseapp.com",
    databaseURL: "https://prototipo-electron.firebaseio.com",
    projectId: "prototipo-electron",
    storageBucket: "prototipo-electron.appspot.com",
    messagingSenderId: "350441246802",
    appId: "1:350441246802:web:3917b7031255bca5418f8b",
    measurementId: "G-JHXDG05DDB"
  },
  senderID: '350441246802'
};